import subprocess
import time
import os
from datetime import datetime

def extract_from_vmstat(vmstat_stdout, index):
    return vmstat_stdout.split('\n')[index].strip().split(' ')[0]

def mem_used(vmstat_stdout):
    return extract_from_vmstat(vmstat_stdout, 1)

def mem_available(vmstat_stdout):
    return extract_from_vmstat(vmstat_stdout, 0)

def get_jobid_node(job_env_var='SLURM_JOB_ID', node_env_var='SLURM_NODELIST'):
    job_id = os.environ[job_env_var]
    node_list = os.environ[node_env_var]

    scontrol_cmd = ['scontrol', 'show', 'hostnames', node_list]
    out = subprocess.check_output(scontrol_cmd)
    node_list = out.decode('utf8').split('\n')[:-1]

    return job_id, node_list

def create_output_file(fname, node_list):
    with open(fname, 'w') as f:
        line = '#All memory size are expressed in MB. The first line is the total available memory\n'
        line += '#Time   ' +  '  '.join(node_list) + '\n'
        f.write(line)

def build_line(node_list, quantity):
    vmstat_cmd = ["vmstat", "-s", "-S", "m"]
    line = datetime.now().strftime("%H:%M:%S") + '   '
    for n in node_list:
        cmd = ['ssh', n] + vmstat_cmd 
        out = subprocess.check_output(cmd)
        line += quantity(out.decode('utf8')) + '   '

    return line + '\n'

def append_line(fname, line):
    with open(fname, 'a+') as f:
        f.write(line)
    

def run(freq=5):
    job_id, node_list = get_jobid_node()
    fname = f'mem_log_{job_id}.dat'
    create_output_file(fname, node_list)
    line = build_line(node_list, mem_available)
    append_line(fname, line)
    
    while 1:
        line = build_line(node_list, mem_used)
        append_line(fname, line)
   
        time.sleep(freq)

if __name__ == "__main__":
    run()
