Memory_logger is a python script that should be launched in your Slurm batch script. It starts with fetching the nodes of your allocation by reading Slurm environment variables. Then it periodically (every 5s) ssh to each compute node and collects the used memory thanks to the system *vmstat* command. Results are written in a file mem_log_<jobid>.dat. The first line of this file represent the maximum available memory for each node.

The fake_mpi_sim.c file is here for testing purpose. Simply compile it with 

```bash
mpicc fake_mpi_sim.c
```

and then launch the job script example

```bash
sbatch job_plafrim
```
