#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <unistd.h>
#include <string.h>

int main (int argc, char **argv)
{
  int rank, size;
  double* A;
  MPI_Init (&argc, &argv);
  MPI_Comm_rank (MPI_COMM_WORLD, &rank);
  MPI_Comm_size (MPI_COMM_WORLD, &size);
  printf( "Hello world from process %d of %d\n", rank, size );
  if(rank == 0)
  {
    sleep(2);
    A = malloc(1e9);
    memset(A, 0, 1e9);
  }
  else if(rank == 1)
  {
    sleep(7);
    A = malloc(3e9);
    memset(A, 0, 3e9);
    sleep(2);
  }

  MPI_Finalize();
  return 0;
}
